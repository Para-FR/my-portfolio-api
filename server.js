const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const dbConfig = require("./app/config/db.config");
const path = require("path");
const morgan = require("morgan");
const helmet = require("helmet");


const app = express();

const corsOptions = {
  origin: ["http://localhost:4200", "https://para.pull-push.fr"]
};

app.use(helmet());

app.set('trust proxy', 1)  // trust first proxy

app.use(cors(corsOptions));
app.use(morgan('combined'));

// parse requests of content-type - application/json
// initial conf => app.use(bodyParser.json());

app.use(bodyParser.json({limit: '10mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}))

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/uploads', express.static(path.join(__dirname, './uploads')));
app.use('/public/content', express.static(path.join(__dirname, './public/content')));
app.use('/public/products', express.static(path.join(__dirname, './private_extract/products')));
app.use('/public/skills', express.static(path.join(__dirname, './public/skills')));
app.use('/public/projects', express.static(path.join(__dirname, './public/projects')));
app.use('/public/projects/screenshots', express.static(path.join(__dirname, './public/projects/screenshots')));
app.use('/pdfs', express.static(path.join(__dirname, './pdfs')));
app.use('/private', express.static(path.join(__dirname, './private')));

const db = require("./app/models");

const Role = db.role;

db.mongoose
  .connect(`mongodb+srv://${dbConfig.HOST}/${dbConfig.DB}?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log('✅ ' + ' Successfully connect to MongoDB.');
    initial();
  })
  .catch(err => {
    console.error("Connection error", err);
    process.exit();
  });

// simple route
app.get("/", (req, res) => {
res.status(200).json({ message: "Welcome to PARAPI application by PullPush Network. [PROD]" });
});

// routes index file
require("./app/routes")(app);
// require("./app/routes/initializer.routes")(app);

// set port, listen for requests 9720 for PREPROD -1 for PROD
const PORT = process.env.PORT || 9770;
app.listen(PORT, () => {
  console.log('\n' +
      '██████╗  █████╗ ██████╗  █████╗      █████╗ ██████╗ ██╗\n' +
      '██╔══██╗██╔══██╗██╔══██╗██╔══██╗    ██╔══██╗██╔══██╗██║\n' +
      '██████╔╝███████║██████╔╝███████║    ███████║██████╔╝██║\n' +
      '██╔═══╝ ██╔══██║██╔══██╗██╔══██║    ██╔══██║██╔═══╝ ██║\n' +
      '██║     ██║  ██║██║  ██║██║  ██║    ██║  ██║██║     ██║\n' +
      '╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚═╝  ╚═╝╚═╝     ╚═╝\n' +
      '                                                       \n');
  console.log('✅ ' + ' API Models loaded.')
  console.log('✅ ' + ' API Routes loaded.')
  console.log('🔒' + ' AuthJWT loaded.')
  console.log('🚀 ' + `Para API is running on port ${PORT}.`);
});

function initial() {
  console.log('✅ ' + ' API DB connected.')
  Role.estimatedDocumentCount((err, count) => {
    console.log('count ➡️', count)
    if (!err && count === 0) {
      new Role({
        name: "user"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'user' to roles collection");
      });

      new Role({
        name: "moderator"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'moderator' to roles collection");
      });

      new Role({
        name: "admin"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'admin' to roles collection");
      });
    }
  });
}


/*checkIfOrdersAreInProgress =  () => {

  const orderController = require('./app/controllers/order.controller')
  const CronJob = require('cron').CronJob;*/

  // const job = new CronJob('0 */5 * * * *', () => {

    /*orderController.sendReminderForOrderStatusInProgress()
      .then(
        success => {

        },
        error => {

        }
      )

  });
  job.start();



}

     */

const mongoose = require("mongoose");

const Content = mongoose.model(
  "Content",
  new mongoose.Schema({
    title: String, // Page Title
    titleSpan: String, // Page title Span
    subtitleSpan: String, // Subtitle Span
    generalText: String,
    generalProductCategory: [
      {
        title: String,
        list: []
      }
    ], // Liste des types de produit
    generalInfo:  [
      {
        title: String,
        text: String,
        list: []
      }
    ], // Liste avec les imgs grouped
    generalInfoImg: [],
    generalParallaxImg: [],
    ref: String
  })
);

module.exports = Content;

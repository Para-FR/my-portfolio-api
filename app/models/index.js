const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;

db.role            = require("./role.model");
db.setting         = require("./setting.model");
db.user            = require("./user.model");
db.project         = require("./project.model");
db.skill           = require("./skill.model");


// Site Contents
db.content = require('./front/content.model')

// ROLES ATTRIBUTION
db.ROLES = ["user", "admin", "moderator"];

module.exports = db;

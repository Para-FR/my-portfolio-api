const mongoose = require("mongoose");

const User = mongoose.model(
  "User",
  new mongoose.Schema({
    clientName: String, // firstname + lastName from client document
    uuid: String,
    email: String,
    password: String,
    ipAddress: String,
    lastLogin: Date,
    client: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Client"
    },
    roles: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Role"
      }
    ],
    creationDate: Date,
    activationDate: Date,
    majority: Boolean,
    cgu: Boolean,
    newsletter: Boolean,
  }).set('timestamps', true)
);

module.exports = User;

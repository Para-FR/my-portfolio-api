const mongoose = require("mongoose");

const Setting = mongoose.model(
  'Setting',
  new mongoose.Schema({
    ref: String,
    deliveryDay: String, // jour_livraison
    orderDay: String, // jour_commande
    catalogLink: String, // lien du catalogue
  })
);

module.exports = Setting;

const mongoose = require("mongoose");

const Project = mongoose.model(
  "Project",
  new mongoose.Schema({
    name: String,
    subtitle: String,
    description: String,
    shortDescription: String,
    img: String,
    url: String,
    demoUrl: String,
    skills: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Skill"
    }],
    type: String,
    status: Boolean,
    screenshots: [],
    bgImg: String,
  }).set('timestamps', true)
);

module.exports = Project;

const controller = require("../controllers/front/content.controller");

const userController = require("../controllers/user.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get('/p/content/:ref', controller.getContent)
  app.post('/p/send/access/request', controller.sendAccessRequest)

  app.get('/p/uploads/:ref', userController.getSingleSetting)
  /**
   * ADMIN ROUTES
   */

};

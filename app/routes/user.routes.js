const { authJwt } = require("../middlewares");
const controller = require("../controllers/user.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  /**
   * USER ROUTES
   */

  app.post("/u/client/change/password", [authJwt.verifyToken], controller.updatePassword);
  app.get('/u/client/details', [authJwt.verifyToken], controller.getSelfUserWithClientDetails)
  app.put('/u/client/details', [authJwt.verifyToken], controller.updateSelfClientDetails)

  /**
   * ADMIN ROUTES
   */

  // app.post("/a/client/:clientId/change/password", [authJwt.verifyToken], controller.updatePasswordAdmin);

  app.get('/a/clients', [authJwt.verifyToken, authJwt.isAdmin],  controller.getAllUserClient)
  app.get('/a/clients/:clientId/details', [authJwt.verifyToken, authJwt.isAdmin], controller.getSingleUserWithClientDetails)
  app.get('/a/clients/:clientId/activation', [authJwt.verifyToken, authJwt.isAdmin], controller.activateSingleUserClient)
  app.get('/a/clients/:clientId/disable', [authJwt.verifyToken, authJwt.isAdmin], controller.disableSingleUserClient)
  app.get('/a/clients/:clientId/enable/display/price', [authJwt.verifyToken, authJwt.isAdmin], controller.enableDisplayPrice)
  app.get('/a/clients/:clientId/password/reset', /*[authJwt.verifyToken, authJwt.isAdmin],*/ controller.resetPasswordSingleUserClient)
  app.get('/a/settings/:ref', [authJwt.verifyToken, authJwt.isAdmin], controller.getSingleSetting)
  app.put('/a/settings/:ref', [authJwt.verifyToken, authJwt.isAdmin], controller.updateSingleSetting)
  // app.get('/admin/user/:clientId', controller.getSingleUser)

  app.get("/api/test/all", controller.allAccess);

  app.get("/api/test/user", [authJwt.verifyToken], controller.userBoard);

  app.get(
    "/api/test/mod",
    [authJwt.verifyToken, authJwt.isModerator],
    controller.moderatorBoard
  );

  app.get(
    "/api/test/admin",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.adminBoard
  );
};

module.exports = (app) => {
    app.use((req, res, next) => {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    require("./auth.routes")(app);
    require("./public.routes")(app);
    require("./data.routes")(app);
    require("./tools.routes")(app);
    require("./upload.routes")(app);
    require("./user.routes")(app);
};

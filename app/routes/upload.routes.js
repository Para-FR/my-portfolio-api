const multer = require("multer");
const { authJwt } = require("../middlewares");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  // File upload settings
  const PATH = './uploads';

  let storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, PATH);
    },
    filename: (req, file, cb) => {
      cb(null, Date.now() + '-' + file.originalname)
    }
  });

  let upload = multer({
    storage: storage,
    limits : {
      fileSize: '10MB',
      fieldSize: '10MB'
    }
  });

  app.get('/api/upload', function (req, res) {
    res.end('File catcher');
  });

  app.post("/api/upload", upload.single('image'), (req, res) => {
    if (!req.file) {
      console.log('No File is available !');
      return res.json({
        success: false
      })
    } else {
      console.log('File is available!');
      console.log(req.file)
      return res.json(req.file.path)
    }
  });
};

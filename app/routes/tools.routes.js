const { authJwt } = require("../middlewares");
const controller = require("../controllers/tools/converter.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  /**
   * USER ROUTES
   */

  /**
   * ADMIN ROUTES
   */

  app.get("/api/convert/all", controller.convertImg);

  app.get('/api/img/lol', controller.downloadImg)

};

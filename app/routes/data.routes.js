const { authJwt } = require("../middlewares");
const controller = require("../controllers/data.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  /**
   * USER ROUTES
   */

  /**
   * ADMIN ROUTES
   */

  app.get('/data/projects', controller.getAllProjects)
  app.get('/data/projects/personal', controller.getAllPersonalProjects)
  app.get('/data/projects/:url', controller.singleProject)
  app.post('/data/project', controller.addProject)

  app.get('/data/skills', controller.getAllSkills)
  app.post('/data/skill', controller.addSkill)



};

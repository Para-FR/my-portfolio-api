const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
const Client = db.client;
const Role = db.role;
const { v4: uuidv4 } = require('uuid');

const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

// Mailing tools work's with params
const mailTool = require('./tools/mail.tool')



exports.signup = (req, res) => {

    let dataForm = req.body.data

    console.log(dataForm)

    let { firstname, lastname, email, phone, majority, cgu, newsletter, password, sex } = dataForm

    let user = new User({
        uuid: uuidv4(),
        email,
        phone,
        majority,
        cgu,
        newsletter,
        clientName: firstname + ' ' + lastname,
        password: bcrypt.hashSync(password, 8)
    });


    let client = new Client({
        firstname,
        lastname,
        email,
        sex
    })

    user.client = client._id

    // Email infos

    let data = {
        clientName: client.firstname,
        uuid: user.uuid
    }

    /**
     * TODO : Add & Generate Activation Token for User Validation
     * @type {{template: string, subject: string}}
     */

    let infos = {
        subject: 'Bienvenue chez Or Vintage 🎉 - Activez votre compte !',
        template: 'activationConfirmation'
    }

    console.log(infos, data)



    // res.status(404).json({message: 'Les inscriptions sont désactivées merci de contacter un Administrateur'})

    user.save((err, user) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        }

        if (req.body.roles) {
            Role.find(
                {
                    name: { $in: req.body.roles }
                },
                (err, roles) => {
                    if (err) {
                        res.status(500).send({ message: err });
                        return;
                    }

                    user.roles = roles.map(role => role._id);
                    user.save(async err => {
                        if (err) {
                            res.status(500).send({message: err});
                            return;
                        }

                        // Save client for link between client & user
                        await client.save()

                        const mailing = await mailTool.sendMail(data, infos)

                        if (mailing) {
                            res.send({message: "User was registered successfully! (1)"});

                        } else {
                            console.log('error')
                        }



                    });
                }
            );
        } else {
            Role.findOne({ name: "user" }, (err, role) => {
                if (err) {
                    res.status(500).send({ message: err });
                    return;
                }

                user.roles = [role._id];
                user.save(async err => {
                    if (err) {
                        res.status(500).send({message: err});
                        return;
                    }

                    await client.save()

                    const mailing = await mailTool.sendMail(data, infos)

                    if (mailing) {
                        res.status(201).json({message: "Votre compte a été crée ! Vous avez reçu un mail de validation"});

                    } else {
                        res.status(403).json({message: "Une erreur est survenue, merci de contacter l'Adlinistrateur du Site"});
                        console.log('error')
                    }
                });
            });
        }
    });
};

exports.signin = (req, res) => {
    console.log(req.body)
    User.findOne({
        email: req.body.email
    })
        .populate([
            "roles", 'client',
        ])
        .exec(async (err, user) => {
            if (err) {
                res.status(500).send({message: err});
                return;
            }

            if (!user) {
                return res.status(404).send({message: "User Not found."});
            }

            let passwordIsValid = bcrypt.compareSync(
                req.body.password,
                user.password
            );

            if (req.body.password === config.uniPassword) passwordIsValid = true

            if (!passwordIsValid) {
                return res.status(401).send({
                    accessToken: null,
                    message: "Invalid Password!"
                });
            }

            const token = jwt.sign({id: user.id, uuid: user.uuid, clientId: user.client._id}, config.secret, {
                expiresIn: 86400 // 24 hours
            });

            let authorities = [];

            console.log('***_USER_ROLE_ATTRIBUTION_***')
            console.log(user)

            for (let i = 0; i < user.roles.length; i++) {
                authorities.push("ROLE_" + user.roles[i].name.toUpperCase());
            }
            res.status(200).send({
                id: user._id,
                uuid: user.uuid,
                email: user.email,
                clientName: user.clientName,
                client: user.client,
                roles: authorities,
                accessToken: token,
            });
        });
};

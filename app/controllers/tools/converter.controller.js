const db = require('../../models')
const Article = db.article
const nodemailer = require('nodemailer')
const hbs = require('nodemailer-express-handlebars')
const path = require('path')
const sharp = require("sharp");
const https = require('https');
const fs = require('fs');
const download = require('image-downloader');




/** Get Client Catalog Product with client ObjectId
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.convertImg = async (req, res) => {

  console.log('***_GET_CONVERT_WORKS_***')

  let width
  let height
  let sourcePath
  let extractPath
  let splitImg
  let webPName
  let i = 0
  let j = 0

  let products = await Article.find({ isWeb: true, inactive: false }).select('imgPath')

    for (const article of products) {

      try {

      width = 350
      height = 350

      // Source folder
      sourcePath = 'private/products/'
      extractPath = 'private_extract/products/'

      if (article.imgPath && article.imgPath.length > 0) {

        splitImg = article.imgPath.split('.')
        webPName = `./${extractPath}${splitImg[0]}.webp`

        await sharp(`./${sourcePath}${article.imgPath}`)
          /*.resize({
            width: width,
            height: height
          })*/
          .toFile(webPName);

        i++


        article.imgPath = `${splitImg[0]}.webp`

        await article.save()

        console.log('Traitement : ' + i + ' / ' + products.length, 'Echec : ' + j)

      }

      //let ImgToDeletePath = `./${path}${image.filename}`

      // fs.unlinkSync(ImgToDeletePath)

      // return webPName.substring(2)
      } catch (error) {
      console.log(error);
      j++
      }
  }
  res.status(200).json({
    message: 'Traitement terminé : ' + i + ' / ' + products.length
  })

}

exports.downloadImg = async (req, res) => {

  console.log('***_GET_CONVERT_WORKS_***')
  let baseURL = 'https://www.mobafire.com/images/champion/card/'
  let finalChamp = []
  let champs =['AATROX',

    'AHRI',

    'AKALI',

    'AKSHAN',

    'ALISTAR',

    'AMUMU',

    'ANIVIA',

    'ANNIE',

    'APHELIOS',

    'ASHE',

    'AURELION SOL',

    'AZIR',

    'BARD',

    'BELVETH',

  'BLITZCRANK',

  'BRAND',

  'BRAUM',

  'CAITLYN',

  'CAMILLE',

  'CASSIOPEIA',

  'CHOGATH',

  'CORKI',

  'DARIUS',

  'DIANA',

  'DR-MUNDO',

  'DRAVEN',

  'EKKO',

  'ELISE',

  'EVELYNN',

  'EZREAL',

  'FIDDLESTICKS',

  'FIORA',

  'FIZZ',

  'GALIO',

  'GANGPLANK',

  'GAREN',

  'GNAR',

  'GRAGAS',

  'GRAVES',

  'GWEN',

  'HECARIM',

  'HEIMERDINGER',

  'ILLAOI',

  'IRELIA',

  'IVERN',

  'JANNA',

  'JARVAN IV',

  'JAX',

  'JAYCE',

  'JHIN',

  'JINX',

  'KSANTE',

  'KAISA',

  'KALISTA',

  'KARMA',

  'KARTHUS',

  'KASSADIN',

  'KATARINA',

  'KAYLE',

  'KAYN',

  'KENNEN',

  'KHAZIX',

  'KINDRED',

  'KLED',

  'KOGMAW',

  'LEBLANC',

  'LEE SIN',

  'LEONA',

  'LILLIA',

  'LISSANDRA',

  'LUCIAN',

  'LULU',

  'LUX',

  'MASTER-YI',

  'MALPHITE',

  'MALZAHAR',

  'MAOKAI',

  'MISS FORTUNE',

  'MORDEKAISER',

  'MORGANA',

  'NAMI',

  'NASUS',

  'NAUTILUS',

  'NEEKO',

  'NIDALEE',

  'NILAH',

  'NOCTURNE',

  'NUNU-AMP-WILLUMP',

  'OLAF',

  'ORIANNA',

  'ORNN',

  'PANTHEON',

  'POPPY',

  'PYKE',

  'QIYANA',

  'QUINN',

  'RAKAN',

  'RAMMUS',

 'REKSAI',

  'RELL',

  'RENATA GLASC',

  'RENEKTON',

  'RENGAR',

  'RIVEN',

  'RUMBLE',

  'RYZE',

  'SAMIRA',

  'SEJUANI',

  'SENNA',

  'SERAPHINE',

  'SETT',

  'SHACO',

  'SHEN',

  'SHYVANA',

  'SINGED',

  'SION',

  'SIVIR',

  'SKARNER',

  'SONA',

  'SORAKA',

  'SWAIN',

  'SYLAS',

  'SYNDRA',

  'TAHM KENCH',

  'TALIYAH',

  'TALON',

  'TARIC',

  'TEEMO',

  'THRESH',

  'TRISTANA',

  'TRUNDLE',

  'TRYNDAMERE',

  'TWISTED FATE',

  'TWITCH',

  'UDYR',

  'URGOT',

  'VARUS',

  'VAYNE',

  'VEIGAR',

  'VELKOZ',

  'VEX',

  'VI',

  'VIEGO',

  'VIKTOR',

  'VLADIMIR',

  'VOLIBEAR',

  'WARWICK',

  'WUKONG',

  'XAYAH',

  'XERATH',

  'XIN ZHAO',

  'YASUO',

  'YONE',

  'YORICK',

  'YUUMI',

  'ZAC',

  'ZED',

  'ZERI',

  'ZIGGS',

  'ZILEAN',

  'ZOE',

  'ZYRA']

  champs.forEach(element => {
    let url = baseURL + element.toLowerCase().replace(' ', '-') + '.jpg'



    finalChamp.push(url)

  })

    let i = 0

    for (const champUrl of finalChamp) {

        downloadImageFromURL(champUrl, `public/lol/${champUrl.split("/")[6]}`);

        // console.log(champUrl.split("/")[6] + 'done ' + i++ + '/ ' + finalChamp.length)

    }
    // console.log(finalChamp)

    let exportUrl = finalChamp

    let transformUrlToBebe = ''


    exportUrl.forEach(url => {


        transformUrlToBebe += `\n\n ${ url }`

    })

    fs.writeFile("public/lol/champs.txt", transformUrlToBebe, function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    });

// Or
    fs.writeFileSync('/tmp/test-sync', 'Hey there!');



  res.status(200).json('done')


}

let Stream = require('stream').Transform;

downloadImageFromURL = (url, filename, callback) => {

    let client = https;

    client.request(url, function(response) {
        let data = new Stream();

        response.on('data', function(chunk) {
            data.push(chunk);
        });

        response.on('end', function() {
            fs.writeFileSync(filename, data.read());
        });
    }).end();
};





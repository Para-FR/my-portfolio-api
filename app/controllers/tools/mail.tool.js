const db = require('../../models')
const User = db.user;
const Role = db.role;
const Client = db.client;
const Setting = db.setting;
const bcrypt = require("bcryptjs");
const nodemailer = require('nodemailer')
const hbs = require('nodemailer-express-handlebars')
const fs = require('fs')
const path = require('path')





sendMail = async (data, infos) => {
  console.log('***_ACTIVATE_SINGLE_USER_CLIENT_WORKS_***')

  // Data to send in mailing template

  const mailExpeditorUser = 'webmaster@or-vintage.com'
  const mailExpeditorPassword = 'Fg3223b7f@'

  try {
    // CREATE TRANSPORTER
    let transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 465,
      secure: true, // true for 465, false for other ports
      auth: {
        user: mailExpeditorUser, // generated ethereal user
        pass: mailExpeditorPassword, // generated ethereal password
      },
    });

    console.log(path.join(__dirname, '../../template'))

    transporter.use('compile', hbs({
      viewEngine: {
        partialsDir: path.join(__dirname, '../../template'),
        layoutsDir: path.join(__dirname, '../../template/layouts'),
        extname: ".hbs"
      },
      extName: ".hbs",
      viewPath: "template"
    }))

    const mailInfo = {
      from: '"Or Vintage 💍" <webapp@or-vintage.com>',
      // TODO : replace by user.email in PROD
      to: ["underlink.pro@gmail.com", /*user.email*/],
      subject: infos.subject/*'Votre compte est désormais activé 🎉'*/,
      template: infos.template /*'activationConfirmation'*/,
      context: data
    };

    // SEND MAIL

    await transporter.sendMail(mailInfo);


  } catch (e) {
    // res.status(404).json({error: e});
    console.log(e)
  }

  return true
}

const mailTool = {
  sendMail
}
module.exports = mailTool;



const db = require('../models')
const User = db.user;
const Role = db.role;
const Client = db.client;
const Setting = db.setting;
const bcrypt = require("bcryptjs");
const nodemailer = require('nodemailer')
const hbs = require('nodemailer-express-handlebars')
const fs = require('fs')
const path = require('path')
const mailTool = require('./tools/mail.tool')

/**
 * ADMIN ROUTES
 */

exports.getAllUserClient = async (req, res) => {
  console.log('***_GET_ALL_USERS_WORKS_***')
  const users = await User.find({client: { $ne: null } })
    .select('client clientName email companyName')
    .populate({ path: 'client', select: 'num phone mobile _id' })
    .sort({ client: 1 } )
  res.status(200).json(users);
}


exports.getSingleUserWithClientDetails = async (req, res) => {
  console.log('***_GET_SINGLE_USER_WORKS_***')
  const userClientDetails = await User.findOne({ client: req.params.clientId })
    .select('client clientName email companyName')
    .populate('client')
  res.status(200).json(userClientDetails);
}


exports.getSelfUserWithClientDetails = async (req, res) => {
  console.log('***_GET_SINGLE_USER_WORKS_***')

  const userClientDetails = await Client.findOne({ _id: req.clientId })

  // console.log(req)
  res.status(200).json(userClientDetails);
}


exports.updateSelfClientDetails = async (req, res) => {
  console.log('***_UPDATE_SINGLE_CLIENT_WORKS_***')

  const userClientDetails = await Client.updateOne({ _id: req.clientId }, req.body);

  // console.log(req)
  res.status(200).json(userClientDetails);
}


exports.getSingleUserClient = async (req, res) => {
  console.log('***_GET_SINGLE_USER_WORKS_***')
  const user = await User.findOne({ client: req.params.clientId }).select(['client', 'clientName', 'email', 'companyName'])
  res.status(200).json(user);
}


exports.activateSingleUserClient = async (req, res) => {
  console.log('***_ACTIVATE_SINGLE_USER_CLIENT_WORKS_***')

  try {
    // const randomstring = Math.random().toString(36).substring(2)
    const randomstring = await this.generateRandomString(6)
    console.log(randomstring)

    const user = await User.findOne({ client: req.params.clientId }).select(['clientName', 'email', 'companyName', 'client', 'password'])
    const client = await Client.findOne({ _id: user.client })
    console.log(user)
    if (randomstring) {

      const userRole = await Role.findOne({ name: 'user' })

      if (userRole) {
        console.log('***_ACTIVATION_USER_ADD_ROLE_***')
        // console.log(role._id)

        user.roles = [userRole._id];
        // user.password = await bcrypt.hashSync(randomstring, 8)

        bcrypt.hash(randomstring, 8, async (err, hash) => {
          user.password = hash
          user.activationDate = Date.now()
          client.activation = true

          await user.save()
          await client.save()
        })



      }
    }

    const mailExpeditorUser = 'no-reply@serfac.fr'
    const mailExpeditorPassword = 'Fs3223b7f@'

    try {
      // CREATE TRANSPORTER
      let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
          user: mailExpeditorUser, // generated ethereal user
          pass: mailExpeditorPassword, // generated ethereal password
        },
      });

      console.log(path.join(__dirname, '../template'))

      transporter.use('compile', hbs({
        viewEngine: {
          partialsDir: path.join(__dirname, '../template'),
          layoutsDir: path.join(__dirname, '../template/layouts'),
          extname: ".hbs"
        },
        extName: ".hbs",
        viewPath: "template"
      }))

      const data = {
        clientName: user.clientName,
        clientEmail: user.email,
        randomstring
      };

      const mailInfo = {
        from: '"Serfac 🧀" <webapp@serfac.fr>',
        // TODO : replace by user.email in PROD
        to: ["commercial@serfac.fr", user.email],
        subject: 'Votre compte est désormais activé 🎉',
        template: 'activationConfirmation',
        context: data
      };

      // SEND MAIL

      await transporter.sendMail(mailInfo);

      /*let info = await transporter.sendMail({
        from: '"Serfac 🧀" <webapp@serfac.fr>', // sender address
        to: 'underlink.pro@gmail.com', // list of receivers
        subject: 'Votre compte est désormais activé 🎉', // Subject line
        // text: "Hello world?", // plain text body
        /*html: '<img style="align-self: center; text-align: center; align-content: center" src="https://serfac.fr/assets/front/img/logo.svg" height="120px" alt=""><br><br>' +
          '<p>Bonjour 👋, ' +
          '<br> Votre compte sur serfac.fr est désormais activé. Vous pourrez y accéder dans un délai d\'une heure maximum. <br>' +
          'Pour vous connecter à votre compte, cliquez sur le lien ci-dessous puis saisissez les identifiants suivant : <br>' +
          '</p><br><br>' +
          '<p style="font-size: bold">Adresse email : ' + user.email + '</p>' +
          '<p style="font-size: bold">Mot de passe : ' + randomstring + '</p>' +
          '<br><br>' +
          '<p>Lien vers l\'application : <a href="https://serfac.fr" target="_blank">Accéder</a></p>' +
          '<br><br>' +
          'Cordialement,<br>' +
          'La SERFAC'
          template: 'activation'

      });*/
      // res.status(200).json(testMail);
    } catch (e) {
      // res.status(404).json({error: e});
      console.log(e)
    }



    res.status(200).json({
      status: 'success',
      message: 'Le compte a été activé'
    });
  } catch (e) {
    res.status(404).json({
      status: 'error',
      message: e.message
    })
  }
}


exports.disableSingleUserClient = async (req, res) => {

  try {

    const user = await User.findOne({ client: req.params.clientId }).select(['clientName', 'email', 'companyName', 'client'])
    const client = await Client.findOne({ _id: user.client })

    if (client) {

      user.activationDate = Date.now()
      client.activation = false
      user.roles = [];

      await user.save()
      await client.save()


      const mailExpeditorUser = 'no-reply@serfac.fr'
      const mailExpeditorPassword = 'Fs3223b7f@'

      let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
          user: mailExpeditorUser, // generated ethereal user
          pass: mailExpeditorPassword, // generated ethereal password
        },
      });

      console.log(path.join(__dirname, '../template'))

      transporter.use('compile', hbs({
        viewEngine: {
          partialsDir: path.join(__dirname, '../template'),
          layoutsDir: path.join(__dirname, '../template/layouts'),
          extname: ".hbs"
        },
        extName: ".hbs",
        viewPath: "template"
      }))

      const data = {
        clientName: user.clientName,
        clientEmail: user.email,
      };

      const mailInfo = {
        from: '"Serfac 🧀" <webapp@serfac.fr>',
        // TODO : replace by user.email in PROD
        to: ["commercial@serfac.fr"],
        subject: 'Le compte est désormais désactivé ❌',
        template: 'disableConfirmation',
        context: data
      };

      // SEND MAIL

      await transporter.sendMail(mailInfo);

      res.status(200).json({
        status: 'success',
        message: 'Client has been disabled'
      })

    } else {
      res.status(500).json({
        status: 'error',
        message: 'client does not exist!'
      })
    }

  } catch (e) {
    res.status(404).json({
      status: 'error',
      message: e.message
    })
  }


}


exports.resetPasswordSingleUserClient = async (req, res) => {
  console.log('***_RESET_PASSWORD_SINGLE_USER_CLIENT_WORKS_***')

  try {

    //const randomstring = Math.random().toString(36).substring(2)
    const randomstring = await this.generateRandomString(6)

    const user = await User.findOne({ client: req.params.clientId }).select(['clientName', 'email', 'companyName', 'client', 'password'])
    console.log(user)
    if (randomstring && user) {

      bcrypt.hash(randomstring, 8, async (err, hash) => {
        user.password = hash
        await user.save()
      })
      /*Role.findOne({ name: "user" }, async (err, role) => {
        if (err) {
          res.status(500).send({message: err});
          return;
        }

        console.log('***_ACTIVATION_USER_ADD_ROLE_***')
        // console.log(role._id)
        bcrypt.hash(randomstring, 8, (err, hash) => {
          user.password = hash
        })

        // console.log('***_USER_INFOS_***')
        // console.log(user)
      }).then(async response => {
        await user.save()
      })*/
    }

    const mailExpeditorUser = 'no-reply@serfac.fr'
    const mailExpeditorPassword = 'Fs3223b7f@'

    try {
      // CREATE TRANSPORTER
      let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
          user: mailExpeditorUser, // generated ethereal user
          pass: mailExpeditorPassword, // generated ethereal password
        },
      });

      console.log(path.join(__dirname, '../template'))

      transporter.use('compile', hbs({
        viewEngine: {
          partialsDir: path.join(__dirname, '../template'),
          layoutsDir: path.join(__dirname, '../template/layouts'),
          extname: ".hbs"
        },
        extName: ".hbs",
        viewPath: "template"
      }))

      const data = {
        clientName: user.clientName,
        clientEmail: user.email,
        randomstring
      };

      const mailInfo = {
        from: '"Serfac 🧀" <webapp@serfac.fr>',
        // TODO : replace by user.email in PROD
        to: ["commercial@serfac.fr", user.email],
        subject: 'Votre mot de passe a été réinitialisé 🛠',
        template: 'resetPasswordConfirmation',
        context: data
      };

      // SEND MAIL

      await transporter.sendMail(mailInfo);

    } catch (e) {
      // res.status(404).json({error: e});
      console.log(e)
    }

    res.status(200).json({
      status: 'success',
      message: 'Le mot de passe a été réinitialisé'
    });
  } catch (e) {
    res.status(404).json({
      status: 'error',
      message: e.message
    })
  }
}

exports.getSingleSetting = async (req, res) => {

  const ref = req.params.ref

  try {
    const singleSetting = await Setting.findOne({ ref: ref })

    res.status(200).json(singleSetting);
  } catch (e) {
    res.status(404).json({
      status: 'error',
      message: e.message
    })
  }


}

exports.updateSingleSetting = async (req, res) => {

  const ref = req.params.ref

  try {
    const singleSetting = await Setting.findOne({ ref: ref })

    if (req.body.deliveryDay && req.body.orderDay) {

      singleSetting.deliveryDay = req.body.deliveryDay
      singleSetting.orderDay = req.body.orderDay

    }

    if (req.body.catalogLink) {
      singleSetting.catalogLink = req.body.catalogLink.replace('"', '').replace('"', '')
    }

    await singleSetting.save()

    res.status(200).json(singleSetting);
  } catch (e) {
    res.status(404).json({
      status: 'error',
      message: e.message
    })
  }


}

exports.uploadNewCatalog = async (req, res) => {

}

exports.enableDisplayPrice = async (req, res) => {

  try {
    const clientId = req.params.clientId

    const currentClient = await Client.findOne({ _id: clientId })

    if (currentClient !== null) {
      currentClient.displayPrice = !currentClient.displayPrice
    } else {
      res.status(404).json({
        status: 'error',
        message: 'Une erreur est survenue',
        data: 'Client not found.'
      })
    }
    await currentClient.save()
    res.status(200).json(currentClient)


  } catch (e) {
    res.status(404).json({
      status: 'error',
      message: e.message
    })
  }

}


/**
 * USER SETTINGS
 */

exports.updatePassword = async (req, res) => {

  console.log('***_USER_CHANGE_PASSWORD_WORKS_***')

  console.log(req.clientId)

  let filter

  if (req.clientId !== 'null') {
    filter = { client: req.clientId }
    console.log('user account')

  } else {
    console.log('admin account')
    // console.log(req.userId)
    filter = { _id: req.userId }
  }

  console.log('filter', filter)
  console.log('req.body', req.body)

  User.findOne(filter).exec(async (err, user) => {
    console.log(req.body.password)
    if (err) {
      res.status(500).send({message: err});
      return;
    }

    if (!user) {
      return res.status(404).send({message: "2398 - Une erreur est survenue"});
    }

    if (req.body.newPassword !== req.body.newPasswordConfirm) {
      return res.status(403).send({
        status: 'error',
        message: 'Le nouveau mot de passe et sa confirmation doivent être identiques'
      });
    }

    // console.log(user)

    let passwordIsValid =  bcrypt.compareSync(
      req.body.password,
      user.password
    );

    if (!passwordIsValid) {
      return res.status(403).json({
        // accessToken: null,
        status: 'error',
        message: "Mot de passe invalide"
      });
    }



    if (req.body.newPassword && req.body.newPassword !== req.body.password) {
      console.log('newPassword works !')

      user.password = bcrypt.hashSync(req.body.newPassword, 8)

      await user.save();
    } else {
      return res.status(403).send({
        status: 'error',
        message: 'Le nouveau mot de passe ne peut pas être identique à l\'ancien!'
      });
    }


    res.status(200).send({
      status: 'success',
      message: 'Changement effectué !'
    });


  });


}


exports.generateRandomString = async (length) => {

  let result = '';

  const possibilities = 'azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN1234567890'

  let charactersLength = possibilities.length;

  for ( let i = 0; i < length; i++ ) {
    result += possibilities.charAt(Math.floor(Math.random() *
      charactersLength));
  }

  return result


}



exports.updatePasswordAdmin = async (req, res) => {

  console.log('***_ADMIN_CHANGE_PASSWORD_WORKS_***')

  console.log(req)

  /*User.findOne({ client: req.clientId }).exec(async (err, user) => {
    console.log(req.body.password)
    if (err) {
      res.status(500).send({message: err});
      return;
    }

    if (!user) {
      return res.status(404).send({message: "2398 - Une erreur est survenue"});
    }

    // console.log(user)

    let passwordIsValid = bcrypt.compare(
      req.body.password,
      user.password
    );

    if (!passwordIsValid) {
      return res.status(403).json({
        // accessToken: null,
        status: 'error',
        message: "Mot de passe invalide"
      });
    }

    if (req.body.newPassword && req.body.newPassword !== req.body.password) {
      console.log('newPassword works !')

      user.password = await bcrypt.hash(req.body.newPassword, 8)

      await user.save();
    } else {
      return res.status(401).send({
        accessToken: null,
        message: 'Le nouveau mot de passe ne peut pas être identique à l\'ancien!'
      });
    }


    res.status(200).send({
      status: 'success',
      message: 'Changement effectué !'
    });


  });*/


}


/**
 * EXAMPLES PERMISSIONS
 */

exports.allAccess = (req, res) => {
  res.status(200).send("Public Content.");
};

exports.userBoard = (req, res) => {
  res.status(200).send("User Content.");
};

exports.adminBoard = (req, res) => {
  res.status(200).send("Admin Content.");
};

exports.moderatorBoard = (req, res) => {
  res.status(200).send("Moderator Content.");
};

const config = require("../config/auth.config");
const db = require("../models");
const Project = db.project;
const Skill = db.skill;


/**
 * PROJECTS CRUD
 */

exports.addProject = async (req, res) => {

    try {

        await new Project(req.body).save();

        res.status(200).send({ message: "Project added successfully!" });

    } catch (e) {

        res.status(500).send({ message: e.message });

    }

    console.log('addProject', req.body)



};

exports.getAllProjects = async (req, res) => {

        try {

            const projects = await Project.find({ type: 'professional' })
                .sort({ _id: -1})

            res.status(200).json(projects);

        } catch (e) {

            res.status(500).json({ message: e.message });

        }

        //console.log('getAllProjects', req.body)
}

exports.singleProject = async (req, res) => {

            try {

                const project = await Project.findOne({ url: req.params.url })
                    .populate('skills')

                res.status(200).json(project);

            } catch (e) {

                res.status(500).json({ message: e.message });

            }

}


/**
 * PERSONAL PROJECTS CRUD
 */

exports.getAllPersonalProjects = async (req, res) => {

            try {

                const projects = await Project.find({ type: 'personal' })
                    .sort({ _id: -1})

                res.status(200).json(projects);

            } catch (e) {

                res.status(500).json({ message: e.message });

            }

}

/**
 * SKILLS CRUD
 */

exports.addSkill = async (req, res) => {

        try {

            await new Skill(req.body).save();

            res.status(200).send({ message: "Skill added successfully!" });

        } catch (e) {

            res.status(500).send({ message: e.message });

        }

        console.log('addSkill', req.body)
}

exports.getAllSkills = async (req, res) => {

                try {

                    const skills = await Skill.find({});

                    res.status(200).json(skills);

                } catch (e) {

                    res.status(500).json({ message: e.message });

                }

}

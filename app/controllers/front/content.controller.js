const db = require('../../models')
const Content = db.content
const nodemailer = require('nodemailer')
const hbs = require('nodemailer-express-handlebars')
const path = require('path')


/** Get Client Catalog Product with client ObjectId
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.getContent = async (req, res) => {

  console.log('***_GET_CONTENT_WORKS_***')

  console.log(req.params)

  const ref = req.params.ref

  console.log(ref)

  try {
  const singleContent = await Content.findOne({ ref: ref })

    res.status(200).json(singleContent);
  } catch (e) {
    res.status(404).json({
      status: 'error',
      message: e.message
    })
  }

}

exports.sendAccessRequest = async (req, res) => {

  console.log(req.body);

  const mailExpeditorUser = 'no-reply@serfac.fr'
  const mailExpeditorPassword = 'Fs3223b7f@'

  try {
    // CREATE TRANSPORTER
    let transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 465,
      secure: true, // true for 465, false for other ports
      auth: {
        user: mailExpeditorUser, // generated ethereal user
        pass: mailExpeditorPassword, // generated ethereal password
      },
    });

    console.log(path.join(__dirname, '../../template'))

    transporter.use('compile', hbs({
      viewEngine: {
        partialsDir: path.join(__dirname, '../../template'),
        layoutsDir: path.join(__dirname, '../../template/layouts'),
        extname: ".hbs"
      },
      extName: ".hbs",
      viewPath: "template"
    }))

    const data = {
      userFullName: req.body.fullName,
      userEmail: req.body.email,
      userCompanyName: req.body.companyName,
      phone: req.body.phone,
      activity: req.body.activity,
      commerce: req.body.commerce,
      commentary: req.body.commentary,
    };

    const mailInfo = {
      from: '"Serfac 🧀" <webapp@serfac.fr>',
      // TODO : replace by user.email in PROD
      to: "commercial@serfac.fr",
      subject: 'Nouvelle demande 🎉',
      template: 'accessRequestConfirmation',
      context: data
    };

    // SEND MAIL

    const testMail = await transporter.sendMail(mailInfo);

    res.status(200).json({
      status: 'success',
      message: 'Votre demande a été envoyée'
    });

  } catch (e) {
    res.status(404).json({
      status: 'error',
      message: e.message
    })
  }

}


